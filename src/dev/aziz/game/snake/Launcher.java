package dev.aziz.game.snake;

public class Launcher {

    public static void main(String[] args){

        int width = 900;
        int height = 600;

        Game game = new Game(width, height);
        game.render();

    }

}
