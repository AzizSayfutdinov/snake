package dev.aziz.game.snake;

import dev.aziz.game.snake.display.Display;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Game {

    private String title = "Snake-Game";
    private int width, height;
    private Display display;

    private BufferStrategy bs;
    private Graphics g;

    public Game(int width, int height){

        this.width = width;
        this.height = height;
        display = new Display(title, width, height);

    }

    public void render(){
        bs = display.getCanvas().getBufferStrategy();

        if(bs == null){
            display.getCanvas().createBufferStrategy(3);
            //bs = display.getCanvas().getBufferStrategy();
            return;
        }

        g = bs.getDrawGraphics();

        g.clearRect(0,0,width, height);
        g.setColor(Color.RED);
        g.fillRect( 0,  0, 1000, 1000);
        bs.show();

    }


}
